# LibreSAT-i

An open source cubesat platform by Libre Space Foundation.

## Getting Started

This repository contains all needed software, hardware and documentation for a complete cubesat satellite. This can be considered a work in progress with regular updates and relatively complex structure to navigate.

## Documentation

The high-level documentation of the platform can be found in the ```dev``` folder of this repository, while it is also browsable [here](https://gitlab.com/librespacefoundation/libresat-i/blob/master/docs/home.md).

## Contributing

Please read [CONTRIBUTING.md](#link_needed) for details on our code of conduct, and the process for submitting pull requests to us.

## Versioning
TBD


## License

Each component of LibreSAT-i is licensed under different libre licenses. Refer to each directory or repository for more details.
